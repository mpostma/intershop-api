declare const _default: {
    name: string;
    type: string;
    commonShipToAddress: {
        type: string;
        city: string;
        countryCode: string;
        postalCode: string;
        street: string;
        phoneBusiness: string;
        firstName: string;
        lastName: string;
        title: string;
        addressName: string;
        id: string;
        country: string;
    };
    commonShippingMethod: {
        type: string;
        shippingTimeMin: number;
        shippingTimeMax: number;
        id: string;
    };
    purchaseCurrency: string;
    shippingBuckets: {
        name: string;
        type: string;
        shippingMethod: {
            name: string;
            type: string;
            shippingTimeMin: number;
            shippingTimeMax: number;
            id: string;
        };
        shipToAddress: {
            type: string;
            city: string;
            countryCode: string;
            postalCode: string;
            street: string;
            phoneBusiness: string;
            firstName: string;
            lastName: string;
            title: string;
            addressName: string;
            id: string;
            country: string;
        };
        lineItems: {
            type: string;
            uri: string;
            attributes: ({
                name: string;
                type: string;
                value: number;
            } | {
                name: string;
                type: string;
                value: string;
            })[];
        }[];
    }[];
    multipleShippmentsSupported: boolean;
    dynamicMessages: never[];
    salesTaxTotalsByTaxRate: {
        name: string;
        type: string;
        amount: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        rate: number;
    }[];
    shippingTaxTotalsByTaxRate: {
        name: string;
        type: string;
        amount: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        rate: number;
    }[];
    totals: {
        name: string;
        type: string;
        basketShippingRebatesTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        basketTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        itemShippingRebatesTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        bucketShippingRebatesTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        dutiesAndSurchargesTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        shippingTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        taxTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        itemTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        basketValueRebatesTotal: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
    };
    id: string;
};
export default _default;
