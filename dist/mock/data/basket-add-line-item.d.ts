import { Types } from '../../models';
declare const _default: {
    type: Types;
    name: string;
    uri: string;
};
export default _default;
