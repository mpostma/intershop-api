import MockAdapter from 'axios-mock-adapter'
import { Observable } from 'rxjs'
import { first } from 'rxjs/operators'

import { defaultHttpHeaders, Http } from '../src'
import { HttpResponse } from '../src/models'

describe('Http', () => {
    let http: Http

    beforeEach(() => {
        http = new Http({
            mock: true,
            host: '',
            production: false
        })

        new MockAdapter(http['_backend'])
            // GET
            .onGet('/test').reply(200, { message: 'GET ok' })
            .onGet('/test/error').reply(500, { message: 'GET fail' })
            // POST
            .onPost('/test').reply(200, { message: 'POST ok' })
            .onPost('/test/error').reply(500, { message: 'POST fail' })
            // PUT
            .onPut('/test').reply(200, { message: 'PUT ok' })
            .onPut('/test/error').reply(500, { message: 'PUT fail' })
            // PATCH
            .onPatch('/test').reply(200, { message: 'PATCH ok' })
            .onPatch('/test/error').reply(500, { message: 'PATCH fail' })
            // DELETE
            .onDelete('/test').reply(200, { message: 'DELETE ok' })
            .onDelete('/test/error').reply(500, { message: 'DELETE fail' })
            // HEAD
            .onHead('/test').reply(200, { message: 'HEAD ok' })
            .onHead('/test/error').reply(500, { message: 'HEAD fail' })
    })

    it('creates', () => {
        expect(http instanceof Http).toBeTruthy()
    })

    describe('defaultHeaders', () => {
        it('gets default backend headers', () => {
            expect(http.defaultHeaders).toEqual(http['_backend'].defaults.headers.common)
        })

        it('uses default headers in each requests', done => {
            const testHeaders = {
                ...defaultHttpHeaders,
                TestHeader: 'test-header-value'
            }
            http.defaultHeaders = testHeaders

            expect(http['_backend'].defaults.headers.common).toEqual(testHeaders)

            http.get('/test').pipe(first()).subscribe(res => {
                expect(res.config.headers).toEqual(testHeaders)
                done()
            })
        })
    })

    describe('REQUEST', () => {
        let request: Observable<HttpResponse<any>>

        beforeEach(() => {
            request = http.request({
                method: 'GET',
                url: '/test'
            })
        })

        it('returns Observable', () => {
            expect(request instanceof Observable).toBeTruthy()
        })
        it('corresponds to mocked data', done => {
            request.pipe(first()).subscribe(res => {
                expect(res.status).toEqual(200)
                expect(res.data).toEqual({ message: 'GET ok' })
                done()
            })
        })
        it('handles error', done => {
            http.get('/test/error').pipe(first()).subscribe(
                () => { },
                error => {
                    expect(error.response.status).toEqual(500)
                    expect(error.response.data).toEqual({ message: 'GET fail' })
                    done()
                }
            )
        })
    })

    describe('GET', () => {
        let request: Observable<HttpResponse<any>>

        beforeEach(() => {
            request = http.get('/test')
        })

        it('returns Observable', () => {
            expect(request instanceof Observable).toBeTruthy()
        })
        it('corresponds to mocked data', done => {
            request.pipe(first()).subscribe(res => {
                expect(res.status).toEqual(200)
                expect(res.data).toEqual({ message: 'GET ok' })
                done()
            })
        })
        it('handles error', done => {
            http.get('/test/error').pipe(first()).subscribe(
                () => { },
                error => {
                    expect(error.response.status).toEqual(500)
                    expect(error.response.data).toEqual({ message: 'GET fail' })
                    done()
                }
            )
        })
    })

    describe('POST', () => {
        let request: Observable<HttpResponse<any>>

        beforeEach(() => {
            request = http.post('/test')
        })

        it('returns Observable', () => {
            expect(request instanceof Observable).toBeTruthy()
        })
        it('corresponds to mocked data', done => {
            request.pipe(first()).subscribe(res => {
                expect(res.status).toEqual(200)
                expect(res.data).toEqual({ message: 'POST ok' })
                done()
            })
        })
        it('handles error', done => {
            http.post('/test/error').pipe(first()).subscribe(
                () => { },
                error => {
                    expect(error.response.status).toEqual(500)
                    expect(error.response.data).toEqual({ message: 'POST fail' })
                    done()
                }
            )
        })
    })

    describe('PUT', () => {
        let request: Observable<HttpResponse<any>>

        beforeEach(() => {
            request = http.put('/test')
        })

        it('returns Observable', () => {
            expect(request instanceof Observable).toBeTruthy()
        })
        it('corresponds to mocked data', done => {
            request.pipe(first()).subscribe(res => {
                expect(res.status).toEqual(200)
                expect(res.data).toEqual({ message: 'PUT ok' })
                done()
            })
        })
        it('handles error', done => {
            http.put('/test/error').pipe(first()).subscribe(
                () => { },
                error => {
                    expect(error.response.status).toEqual(500)
                    expect(error.response.data).toEqual({ message: 'PUT fail' })
                    done()
                }
            )
        })
    })

    describe('PATCH', () => {
        let request: Observable<HttpResponse<any>>

        beforeEach(() => {
            request = http.patch('/test')
        })

        it('returns Observable', () => {
            expect(request instanceof Observable).toBeTruthy()
        })
        it('corresponds to mocked data', done => {
            request.pipe(first()).subscribe(res => {
                expect(res.status).toEqual(200)
                expect(res.data).toEqual({ message: 'PATCH ok' })
                done()
            })
        })
        it('handles error', done => {
            http.patch('/test/error').pipe(first()).subscribe(
                () => { },
                error => {
                    expect(error.response.status).toEqual(500)
                    expect(error.response.data).toEqual({ message: 'PATCH fail' })
                    done()
                }
            )
        })
    })

    describe('DELETE', () => {
        let request: Observable<HttpResponse<any>>

        beforeEach(() => {
            request = http.delete('/test')
        })

        it('returns Observable', () => {
            expect(request instanceof Observable).toBeTruthy()
        })
        it('corresponds to mocked data', done => {
            request.pipe(first()).subscribe(res => {
                expect(res.status).toEqual(200)
                expect(res.data).toEqual({ message: 'DELETE ok' })
                done()
            })
        })
        it('handles error', done => {
            http.delete('/test/error').pipe(first()).subscribe(
                () => { },
                error => {
                    expect(error.response.status).toEqual(500)
                    expect(error.response.data).toEqual({ message: 'DELETE fail' })
                    done()
                }
            )
        })
    })

    describe('HEAD', () => {
        let request: Observable<HttpResponse<any>>

        beforeEach(() => {
            request = http.head('/test')
        })

        it('returns Observable', () => {
            expect(request instanceof Observable).toBeTruthy()
        })
        it('corresponds to mocked data', done => {
            request.pipe(first()).subscribe(res => {
                expect(res.status).toEqual(200)
                expect(res.data).toEqual({ message: 'HEAD ok' })
                done()
            })
        })
        it('handles error', done => {
            http.head('/test/error').pipe(first()).subscribe(
                () => { },
                error => {
                    expect(error.response.status).toEqual(500)
                    expect(error.response.data).toEqual({ message: 'HEAD fail' })
                    done()
                }
            )
        })
    })
})
