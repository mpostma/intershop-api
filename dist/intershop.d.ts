import { Config } from './config';
import { Http } from './http';
import { HttpError } from './models';
import { BasketRepository, CategoryRepository, CommonRepository, CustomerRepository, ProductRepository, StoreLocationRepository } from './repositories';
export declare class Intershop {
    readonly Basket: BasketRepository;
    readonly Category: CategoryRepository;
    readonly Common: CommonRepository;
    readonly Customer: CustomerRepository;
    readonly StoreLocation: StoreLocationRepository;
    readonly Product: ProductRepository;
    readonly http: Http;
    private _config;
    config: Config;
    constructor(options: Config);
    interceptError(callback: (error: HttpError) => any | undefined): void;
    private registerMock;
}
