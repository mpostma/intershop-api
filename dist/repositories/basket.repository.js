"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var repository_1 = require("../repository");
var BasketRepository = (function (_super) {
    __extends(BasketRepository, _super);
    function BasketRepository() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BasketRepository.prototype.create = function (options) {
        if (options === void 0) { options = {}; }
        return this.http.post('/baskets', options);
    };
    BasketRepository.prototype.activeBasket = function (options) {
        if (options === void 0) { options = {}; }
        return this.http.get('/baskets/-', options);
    };
    BasketRepository.prototype.getById = function (id, options) {
        if (options === void 0) { options = {}; }
        return this.http.get("/baskets/" + id, options);
    };
    BasketRepository.prototype.lineItems = function (basket, options) {
        if (options === void 0) { options = {}; }
        return this.http.get("/baskets/" + basket.id + "/items", options);
    };
    BasketRepository.prototype.getItem = function (basket, item, options) {
        if (options === void 0) { options = {}; }
        return this.http.get("/baskets/" + basket.id + "/items/" + item.id, options);
    };
    BasketRepository.prototype.addItem = function (basket, items, options) {
        if (items === void 0) { items = []; }
        if (options === void 0) { options = {}; }
        return this.http.post("/baskets/" + basket.id + "/items", { elements: items }, options);
    };
    BasketRepository.prototype.updateItem = function (basket, item, data, options) {
        if (data === void 0) { data = {}; }
        if (options === void 0) { options = {}; }
        return this.http.put("/baskets/" + basket.id + "/items/" + item.id, data, options);
    };
    BasketRepository.prototype.removeItem = function (basket, item, options) {
        if (options === void 0) { options = {}; }
        return this.http.delete("/baskets/" + basket.id + "/items/" + item.id, options);
    };
    BasketRepository.prototype.addPayment = function (basket, payment, options) {
        if (options === void 0) { options = {}; }
        return this.http.post("/baskets/" + basket.id + "/payments", payment, options);
    };
    return BasketRepository;
}(repository_1.Repository));
exports.BasketRepository = BasketRepository;
