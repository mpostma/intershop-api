import { Observable } from 'rxjs'

import {
    Basket,
    BasketLineItem,
    BasketSkuItem,
    HttpResponse,
    LinkObject,
    Payment,
    RequestOptions,
    ResourceCollection,
} from '../models'
import { Repository } from '../repository'

export class BasketRepository extends Repository {
    /**
     * Creates a new basket.
     *
     * @http POST /baskets
     * @docs https://support.intershop.com/kb/index.php/Display/25184E
     * @version <7.8
     */
    create(options: RequestOptions = {}) {
        return this.http.post<LinkObject>('/baskets', options)
    }

    /**
     * Get the active basket
     *
     * @http GET /baskets/-
     * @docs https://support.intershop.com/kb/index.php/Display/Y25878
     * @version <7.8
     */
    activeBasket(options: RequestOptions = {}): Observable<HttpResponse<Basket>> {
        return this.http.get('/baskets/-', options)
    }

    /**
     * Get basket by ID
     *
     * @http GET /basket/<basket-id>
     * @docs https://support.intershop.com/kb/index.php/Display/2579B4
     * @version <7.7
     */
    getById(id: string, options: RequestOptions = {}) {
        return this.http.get<Basket>(`/baskets/${id}`, options)
    }

    /**
     * Gets all line items contained in the given basket.
     *
     * @http GET /baskets/<basket-id>/items
     * @docs https://support.intershop.com/kb/index.php/Display/2T5215
     * @version <7.8
     */
    lineItems(basket: Basket, options: RequestOptions = {}): Observable<HttpResponse<ResourceCollection<BasketLineItem>>> {
        return this.http.get(`/baskets/${basket.id}/items`, options)
    }

    /**
     * Get's a single basket line item.
     *
     * @http GET /baskets/<basket-id>/items/<item-id>
     */
    getItem(basket: Basket, item: BasketLineItem, options: RequestOptions = {}) {
        return this.http.get<BasketLineItem>(`/baskets/${basket.id}/items/${item.id}`, options)
    }

    /**
     * Adds a list of items to an existing cart.
     *
     * @http POST /baskets/<basket-id>/items
     * @docs https://support.intershop.com/kb/index.php/Display/25E185
     * @version 7.4-7.6
     */
    addItem(basket: Basket, items: BasketSkuItem[] = [], options: RequestOptions = {}) {
        return this.http.post<LinkObject>(`/baskets/${basket.id}/items`,
            { elements: items },
            options
        )
    }

    /**
     * Sets line item related data.
     *
     * @http PUT /baskets/<basket-id>/items/<item-id>
     * @docs https://support.intershop.com/kb/index.php/Display/25790P
     * @version <7.6
     */
    updateItem(basket: Basket, item: BasketLineItem, data: any = {}, options: RequestOptions = {}) {
        return this.http.put<LinkObject>(`/baskets/${basket.id}/items/${item.id}`, data, options)
    }

    /**
     * Removes the specified line item from basket.
     *
     * @http DELETE /baskets/<basket-id>/items/<item-id>
     * @docs https://support.intershop.com/kb/index.php/Display/2S2614
     * @version <7.8
     */
    removeItem(basket: Basket, item: BasketLineItem, options: RequestOptions = {}) {
        return this.http.delete<Basket>(`/baskets/${basket.id}/items/${item.id}`, options)
    }

    /**
     * Adds a payment method to the cart.
     *
     * @http POST /baskets/<basket-id>/payments
     * @docs https://support.intershop.com/kb/index.php/Display/2579U5
     * @version <7.5
     */
    addPayment(basket: Basket, payment: Payment, options: RequestOptions = {}) {
        return this.http.post<LinkObject>(`/baskets/${basket.id}/payments`, payment, options)
    }
}
