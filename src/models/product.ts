export interface Product {

}

export interface ProductSearchParams {
    attrs?: string
    amount?: number
    offset?: number
    sortKey?: 'name-asc' | 'name-desc' | 'value-asc' | 'value-desc'
    pageable?: number
    searchTerm?: string
    returnSortKeys?: boolean
    total?: number

}
