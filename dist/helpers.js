"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function isObject(value) {
    return typeof value === 'object' && !!value && !Array.isArray(value);
}
exports.isObject = isObject;
function randomUuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0;
        var v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
exports.randomUuid = randomUuid;
function randomBetween(min, max, round) {
    if (min === void 0) { min = 1; }
    if (max === void 0) { max = 100; }
    if (round === void 0) { round = true; }
    return round
        ? Math.max(min, Math.floor(Math.random() * max))
        : Math.max(min, Math.random() * max);
}
exports.randomBetween = randomBetween;
function randomFromArray(arr) {
    return arr[Math.floor(Math.random() * arr.length)];
}
exports.randomFromArray = randomFromArray;
