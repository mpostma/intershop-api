import { ResourceCollection, StoreLocation } from '../models';
import { Repository } from '../repository';
export declare class StoreLocationRepository extends Repository {
    query(params?: {
        city?: string;
        postalCode?: string;
        countryCode?: string;
    }): import("rxjs").Observable<import("../models").HttpResponse<ResourceCollection<StoreLocation>>>;
}
