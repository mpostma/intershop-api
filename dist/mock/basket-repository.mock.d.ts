import { Basket, BasketLineItem, RequestOptions } from '../models';
import { BaseMock } from './base.mock';
export declare class BasketRepositoryMock extends BaseMock {
    create(): void;
    activeBasket(): void;
    lineItems(basket: Basket): void;
    getItem(basket: Basket, item: BasketLineItem): void;
    updateItem(basketId: string, itemId: string, data?: any): void;
    removeItem(basketId: string, item: BasketLineItem, options?: RequestOptions): void;
}
