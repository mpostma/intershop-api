import { BaseMock } from '../../src/mock/base.mock'
import { Repository } from '../../src/repository'

describe('BaseMock', () => {
    class TestRepository extends Repository {
        method1(id: string) {
            return this.http.get('/test/' + id)
        }
        method2() { }
    }
    class TestRepositoryMock extends BaseMock {
        method1(id: string) {
            this.mock.onGet(/\/test\/+/).reply(204, {
                test: 'TEST',
                id
            })
        }
    }

    let testRepository: TestRepository
    let testRepositoryMock: TestRepositoryMock

    beforeEach(() => {
        testRepository = new TestRepository()
        testRepositoryMock = new TestRepositoryMock(testRepository)
    })

    it('it creates', () => {
        expect(testRepositoryMock).toBeTruthy()
    })

    describe('replace methods', () => {
        it('runs mocked method', done => {
            testRepository.method1('TestID').subscribe(res => {
                expect((res.data as any).test).toEqual('TEST')
                expect((res.data as any).id).toEqual('TestID')
                expect(res.status).toEqual(204)
                done()
            })
        })
    })
})
