// import { MockRepository } from '../../src/decorators'

// describe('MockRepository', () => {
//     it('returns a function', () => {
//         expect(typeof MockRepository).toEqual('function')
//     })

//     describe('overrides methods', () => {
//         class TestRepositoryMock {
//             method1() {
//                 it('method1 called by mock', () => {
//                     // expect(true).toBeTruthy()
//                 })
//             }
//             method2() {
//                 it('method2 called by mock', () => {
//                     // expect(true).toBeTruthy()
//                 })
//             }
//         }

//         @MockRepository(TestRepositoryMock)
//         class TestRepository {
//             method1() {
//                 it('method1 NOT called by mock', () => {
//                     expect(false).toBeTruthy()
//                 })
//             }
//             method2() {
//                 it('method2 NOT called by mock', () => {
//                     expect(false).toBeTruthy()
//                 })
//             }
//             method3() {
//                 it('method3 has been called by repository', () => {
//                     // expect(true).toBeTruthy()
//                 })
//             }
//         }

//         const testInstance = new TestRepository()

//         testInstance.method1()
//         testInstance.method2()
//         testInstance.method3()
//     })
// })
