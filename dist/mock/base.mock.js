"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BaseMock = (function () {
    function BaseMock(repository) {
        var _this = this;
        this.mock = repository['http']['_mock'];
        if (repository.config.mock) {
            var blacklist_1 = ['constructor', 'mock', 'repository'];
            var keys = Object.keys(this.constructor.prototype).filter(function (k) { return !blacklist_1.includes(k); });
            var repoKeys = Object.keys(repository.constructor.prototype).filter(function (k) { return !blacklist_1.includes(k); });
            var _loop_1 = function (key) {
                if (repoKeys.includes(key)) {
                    var originFn_1 = repository[key];
                    var mockFn_1 = this_1[key];
                    Object.defineProperty(repository, key, {
                        value: function () {
                            var args = [];
                            for (var _i = 0; _i < arguments.length; _i++) {
                                args[_i] = arguments[_i];
                            }
                            mockFn_1.call.apply(mockFn_1, [_this].concat(args));
                            return originFn_1.call.apply(originFn_1, [repository].concat(args));
                        },
                    });
                }
            };
            var this_1 = this;
            for (var _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
                var key = keys_1[_i];
                _loop_1(key);
            }
        }
        else {
            this.mock.restore();
        }
    }
    return BaseMock;
}());
exports.BaseMock = BaseMock;
