"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var helpers_1 = require("../helpers");
var repository_1 = require("../repository");
var CategoryRepository = (function (_super) {
    __extends(CategoryRepository, _super);
    function CategoryRepository() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CategoryRepository.prototype.getAll = function (params) {
        return this.http.get('/categories', { params: params });
    };
    CategoryRepository.prototype.getById = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var ids = args.filter(function (a) { return typeof a === 'string'; });
        var params = (args.find(function (a) { return helpers_1.isObject(a); }) || {});
        return this.http.get("/categories/" + ids.join('/'), { params: params });
    };
    return CategoryRepository;
}(repository_1.Repository));
exports.CategoryRepository = CategoryRepository;
