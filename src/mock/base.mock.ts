import { Mock } from '../models'
import { Repository } from '../repository'

export class BaseMock {
    readonly mock: Mock

    constructor(repository: Repository
    ) {
        this.mock = repository['http']['_mock']!!

        if (repository.config.mock) {
            const blacklist = ['constructor', 'mock', 'repository']
            const keys = Object.keys(this.constructor.prototype).filter(k => !blacklist.includes(k))
            const repoKeys = Object.keys(repository.constructor.prototype).filter(k => !blacklist.includes(k))

            for (const key of keys) {
                if (repoKeys.includes(key)) {
                    type RepositoryFn<T> = (this: T, ...args: any[]) => any
                    const originFn = (repository as any)[key] as RepositoryFn<Repository>
                    const mockFn = (this as any)[key] as RepositoryFn<BaseMock>

                    Object.defineProperty(repository, key, {
                        value: (...args: any[]) => {
                            mockFn.call(this, ...args)
                            return originFn.call(repository, ...args)
                        },
                    })
                }
            }
        } else {
            this.mock.restore()
        }
    }
}
