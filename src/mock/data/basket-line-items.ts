export default {
    elements: [
        {
            name: 'Pijpklem compleet 12x12mm 5stuks',
            type: 'BasketLineItem',
            quantity: {
                type: 'Quantity',
                unit: 'SET',
                value: 1
            },
            price: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 14.28
            },
            singleBasePrice: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 14.28
            },
            shippingTaxes: [
                {
                    name: 'name',
                    type: 'AppliedTax',
                    amount: {
                        type: 'Money',
                        currencyMnemonic: 'EUR',
                        value: 0.56
                    },
                    rate: 0.21
                }
            ],
            salesTaxes: [
                {
                    name: 'name',
                    type: 'AppliedTax',
                    amount: {
                        type: 'Money',
                        currencyMnemonic: 'EUR',
                        value: 3.00
                    },
                    rate: 0.21
                }
            ],
            totals: {
                name: 'basketLineItemTotals',
                type: 'BasketLineItemTotalsRO',
                salesTaxTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 3.00
                },
                shippingTaxTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 0.56
                },
                shippingTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 2.69
                },
                total: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 14.28
                }
            },
            isHiddenGift: false,
            isFreeGift: false,
            variationProduct: false,
            bundleProduct: false,
            availability: true,
            inStock: true,
            product: {
                type: 'Link',
                title: '86271',
                uri: 'org-webshop-Site/indi-b2b/products/86271'
            },
            position: 1,
            id: 'WYMKgiOhPHIAAAFqURU74bIN'
        },
        {
            name: 'Pijpklem compleet 6x6mm 1stuks',
            type: 'BasketLineItem',
            quantity: {
                type: 'Quantity',
                unit: 'SET',
                value: 5
            },
            price: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 16.85
            },
            singleBasePrice: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 3.37
            },
            shippingTaxes: [
                {
                    name: 'name',
                    type: 'AppliedTax',
                    amount: {
                        type: 'Money',
                        currencyMnemonic: 'EUR',
                        value: 0.56
                    },
                    rate: 0.21
                }
            ],
            salesTaxes: [
                {
                    name: 'name',
                    type: 'AppliedTax',
                    amount: {
                        type: 'Money',
                        currencyMnemonic: 'EUR',
                        value: 3.54
                    },
                    rate: 0.21
                }
            ],
            totals: {
                name: 'basketLineItemTotals',
                type: 'BasketLineItemTotalsRO',
                salesTaxTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 3.54
                },
                shippingTaxTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 0.56
                },
                shippingTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 2.69
                },
                total: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 16.85
                }
            },
            isHiddenGift: false,
            isFreeGift: false,
            variationProduct: false,
            bundleProduct: false,
            availability: true,
            inStock: true,
            product: {
                type: 'Link',
                title: '86246',
                uri: 'org-webshop-Site/indi-b2b/products/86246'
            },
            position: 2,
            id: 'GkQKgiOhq74AAAFqEJI74bIf'
        },
        {
            name: 'Aluminium leidingklem compleet C1 10mm',
            type: 'BasketLineItem',
            quantity: {
                type: 'Quantity',
                unit: 'EA',
                value: 5
            },
            price: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 53.85
            },
            singleBasePrice: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 10.77
            },
            shippingTaxes: [
                {
                    name: 'name',
                    type: 'AppliedTax',
                    amount: {
                        type: 'Money',
                        currencyMnemonic: 'EUR',
                        value: 0.56
                    },
                    rate: 0.21
                }
            ],
            salesTaxes: [
                {
                    name: 'name',
                    type: 'AppliedTax',
                    amount: {
                        type: 'Money',
                        currencyMnemonic: 'EUR',
                        value: 11.31
                    },
                    rate: 0.21
                }
            ],
            totals: {
                name: 'basketLineItemTotals',
                type: 'BasketLineItemTotalsRO',
                salesTaxTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 11.31
                },
                shippingTaxTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 0.56
                },
                shippingTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 2.69
                },
                total: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 53.85
                }
            },
            isHiddenGift: false,
            isFreeGift: false,
            variationProduct: false,
            bundleProduct: false,
            availability: true,
            inStock: true,
            product: {
                type: 'Link',
                title: '86267',
                uri: 'org-webshop-Site/indi-b2b/products/86267'
            },
            position: 3,
            id: '.AsKgiOhikwAAAFqDsU74bIf'
        },
        {
            name: 'Aluminium leidingklem compleet C1 12mm',
            type: 'BasketLineItem',
            quantity: {
                type: 'Quantity',
                unit: 'EA',
                value: 5
            },
            price: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 54.65
            },
            singleBasePrice: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 10.93
            },
            shippingTaxes: [
                {
                    name: 'name',
                    type: 'AppliedTax',
                    amount: {
                        type: 'Money',
                        currencyMnemonic: 'EUR',
                        value: 0.56
                    },
                    rate: 0.21
                }
            ],
            salesTaxes: [
                {
                    name: 'name',
                    type: 'AppliedTax',
                    amount: {
                        type: 'Money',
                        currencyMnemonic: 'EUR',
                        value: 11.48
                    },
                    rate: 0.21
                }
            ],
            totals: {
                name: 'basketLineItemTotals',
                type: 'BasketLineItemTotalsRO',
                salesTaxTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 11.48
                },
                shippingTaxTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 0.56
                },
                shippingTotal: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 2.68
                },
                total: {
                    type: 'Money',
                    currencyMnemonic: 'EUR',
                    value: 54.65
                }
            },
            isHiddenGift: false,
            isFreeGift: false,
            variationProduct: false,
            bundleProduct: false,
            availability: true,
            inStock: true,
            product: {
                type: 'Link',
                title: '86274',
                uri: 'org-webshop-Site/indi-b2b/products/86274'
            },
            position: 4,
            id: 'D58KgiOhcw0AAAFqv8w74bIf'
        }
    ],
    type: 'BasketLineItems',
    name: 'basketLineItems'
}
