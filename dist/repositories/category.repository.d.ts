import { Category, CategoryGetParams } from '../models';
import { Repository } from '../repository';
export declare class CategoryRepository extends Repository {
    getAll(params?: Partial<CategoryGetParams>): import("rxjs").Observable<import("../models").HttpResponse<Category[]>>;
    getById(...args: Array<string | Partial<CategoryGetParams>>): import("rxjs").Observable<import("../models").HttpResponse<Category>>;
}
