import { Types } from '../../../src'
import { basketLineItemFactory } from '../../../src/mock/factories'
import { isValidQuantity } from '../../helpers.spec'

describe('Basket factories', () => {
    describe('basketLineItemFactory', () => {
        const basketLineItem = basketLineItemFactory()

        it('defines type', () => {
            expect(basketLineItem.type).toEqual(Types.BasketLineItem)
        })

        describe('quantity', () => {
            isValidQuantity(basketLineItem.quantity)
        })
    })
})
