export interface Config {
    host: string
    production: boolean
    mock: boolean
    vat?: number
    currency?: string
    debug?: boolean
}

export const defaultConfig: Required<Config> = {
    host: '',
    production: false,
    mock: true,
    debug: true,
    vat: 21,
    currency: 'EUR'
}
