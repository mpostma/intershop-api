"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Chance = require("chance");
var helpers_1 = require("../../helpers");
var models_1 = require("../../models");
function moneyFactory(value, currencyMnemonic) {
    if (value === void 0) { value = helpers_1.randomBetween(1, 1000, false); }
    if (currencyMnemonic === void 0) { currencyMnemonic = 'EUR'; }
    return {
        type: models_1.Types.Money,
        value: value,
        currencyMnemonic: currencyMnemonic,
    };
}
exports.moneyFactory = moneyFactory;
function quantityFactory(value) {
    if (value === void 0) { value = helpers_1.randomBetween(1, 10, true); }
    return {
        type: models_1.Types.Quantity,
        unit: 'SET',
        value: value
    };
}
exports.quantityFactory = quantityFactory;
function appliedTaxFactory(value, rate) {
    if (value === void 0) { value = helpers_1.randomBetween(1, 1000); }
    if (rate === void 0) { rate = 0.21; }
    return {
        type: models_1.Types.AppliedTax,
        amount: moneyFactory(value * rate),
        name: models_1.Types.AppliedTax,
        rate: rate
    };
}
exports.appliedTaxFactory = appliedTaxFactory;
function addressFactory(args) {
    var city = Chance().city();
    var street = Chance().street();
    var firstName = Chance().first();
    var lastName = Chance().last();
    return __assign({ type: models_1.Types.Address, city: city, countryCode: 'NA', postalCode: Chance().zip(), street: street, phoneBusiness: Chance().phone(), firstName: firstName,
        lastName: lastName, title: Chance().suffix(), addressName: firstName + " " + lastName + ", " + street + " 1337, " + city, id: Chance().guid(), country: Chance().country() }, args);
}
exports.addressFactory = addressFactory;
