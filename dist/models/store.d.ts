import { Types } from './types';
export interface StoreLocation {
    type: Types.StoreLocation;
    name: string;
    address: string;
    address2: string;
    country: string;
    countryCode: string;
    postalCode: string;
    phoneBusiness: string;
    city: string;
    fax: string;
    email: string;
}
