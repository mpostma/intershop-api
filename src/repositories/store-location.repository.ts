import { ResourceCollection, StoreLocation } from '../models'
import { Repository } from '../repository'

export class StoreLocationRepository extends Repository {
    /**
     * Query store locations or returns all by default.
     *
     * @http GET /stores?city{}=&postalCode={}&countryCode={}
     * @docs https://support.intershop.com/kb/index.php/Display/P25031
     * @version <7.5
     */
    query(params?: { city?: string, postalCode?: string, countryCode?: string }) {
        return this.http.get<ResourceCollection<StoreLocation>>('stores', { params })
    }
}
