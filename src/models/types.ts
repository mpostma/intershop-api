export enum Types {
    Address = 'Address',
    AppliedRebate = 'AppliedRebate',
    AppliedTax = 'AppliedTax',
    Basket = 'Basket',
    BasketLineItem = 'BasketLineItem',
    BasketLineItemTotals = 'BasketLineItemTotalsRO',
    BasketShippingBucket = 'BasketShippingBucket',
    BasketShippingMethod = 'BasketShippingMethod',
    BasketTotals = 'BasketTotals',
    Category = 'Category',
    Customer = 'Customer',
    Image = 'Image',
    Link = 'Link',
    Money = 'Money',
    Payment = 'Payment',
    Quantity = 'Quantity',
    ResourceCollection = 'ResourceCollection',
    SMBCustomer = 'SMBCustomer',
    StoreLocation = 'StoreLocation',
    User = 'User',
}

export enum RebateTypes {
    OrderValueOffDiscount = 'OrderValueOffDiscount',
    ShippingPercentageOffDiscount = 'ShippingPercentageOffDiscount'
}
