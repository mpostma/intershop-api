import { Money, Quantity, Types } from '../src'

export function isValidMoney(money: Money) {
    it('defines type', () => {
        expect(money.type).toEqual(Types.Money)
    })

    it('returns valid output', () => {
        expect(money.currencyMnemonic).toEqual('EUR')
        expect(typeof money.value).toEqual('number')
    })
}

export function isValidQuantity(quantity: Quantity) {
    it('defines type', () => {
        expect(quantity.type).toEqual(Types.Quantity)
    })

    it('returns valid output', () => {
        expect(quantity.unit).toEqual('SET')
        expect(typeof quantity.value).toEqual('number')
    })
}
