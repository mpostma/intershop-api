import { defaultConfig } from '../src/config'

describe('config', () => {
    describe('DefaultConfig', () => {
        it('host', () => {
            expect(typeof defaultConfig.host).toEqual('string')
        })
        it('mock', () => {
            expect(typeof defaultConfig.mock).toEqual('boolean')
        })
        it('production', () => {
            expect(typeof defaultConfig.production).toEqual('boolean')
        })
        it('vat', () => {
            expect(typeof defaultConfig.vat).toEqual('number')
        })
        it('currency', () => {
            expect(typeof defaultConfig.currency).toEqual('string')
        })
    })
})
