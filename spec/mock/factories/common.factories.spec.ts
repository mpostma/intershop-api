import { Types } from '../../../src'
import { appliedTaxFactory, moneyFactory, quantityFactory } from '../../../src/mock/factories'
import { isValidMoney, isValidQuantity } from '../../helpers.spec'

describe('Common factories', () => {
    describe('quantityFactory', () => {
        let quantity = quantityFactory()

        isValidQuantity(quantity)

        it('when value given', () => {
            quantity = quantityFactory(100)
            expect(quantity.value).toEqual(100)
        })
    })

    describe('moneyFactory', () => {
        isValidMoney(moneyFactory())

        it('when value given', () => {
            const money = moneyFactory(100)
            expect(money.value).toEqual(100)
        })

        it('when currencyMnemonic given', () => {
            const money = moneyFactory(100, 'CURRENCY')
            expect(money.currencyMnemonic).toEqual('CURRENCY')
        })
    })

    describe('appliedTaxFactory', () => {
        let appliedTax = appliedTaxFactory()

        it('defines type', () => {
            expect(appliedTax.type).toEqual(Types.AppliedTax)
        })

        describe('amount', () => {
            isValidMoney(appliedTax.amount)
        })

        it('when value given', () => {
            appliedTax = appliedTaxFactory(100)
            expect(appliedTax.amount.value).toEqual(21)
        })

        it('when rate given', () => {
            appliedTax = appliedTaxFactory(100, .9)
            expect(appliedTax.rate).toEqual(.9)
            expect(appliedTax.amount.value).toEqual(90)
        })
    })
})
