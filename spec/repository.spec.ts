import { Http } from '../src'
import { Config } from '../src/config'
import { Repository } from '../src/repository'

describe('Repository', () => {
    let apiConfig: Config
    let httpClient: Http
    let repository: Repository

    beforeEach(() => {
        apiConfig = {
            host: 'http://test.com',
            production: false,
            mock: false,
        }
        httpClient = new Http(apiConfig)
        repository = new Repository(httpClient)
    })

    it('creates', () => {
        expect(repository instanceof Repository).toBeTruthy()
    })

    describe('extension', () => {
        class TestRepository extends Repository { }

        let testRepository: TestRepository

        beforeEach(() => {
            testRepository = new TestRepository(httpClient)
        })

        it('creates', () => {
            expect(testRepository).toBeTruthy()
        })
    })
})
