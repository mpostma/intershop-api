import { Address } from './common';
import { Types } from './types';
export interface NewCustomer {
    customerNo: 'NewCustomer';
    title: string;
    firstName: string;
    lastName: string;
    birthday: string;
    phoneHome: string;
    phoneBusiness: string;
    phoneMobile: string;
    fax: string;
    email: string;
    preferredLanguage: string;
    credentials: {
        login: string;
        password: string;
        securityQuestion: string;
        securityQuestionAnswer: string;
    };
    address: Address;
}
export interface Customer {
    type: Types.Customer;
    phoneHome: string;
    firstName: string;
    lastName: string;
    title: string;
    phoneBusiness: string;
    email: string;
    phoneMobile: string;
    customerNo: string;
}
export interface SMBCustomer {
    type: Types.SMBCustomer;
    companyName: string;
    companyName2: string;
    taxationID: string;
    industry: string;
    description: string;
    customerNo: string;
}
export interface BusinessCustomer {
    type: Types.User;
    name: string;
    phoneBusiness: string;
    phoneMobile: string;
    phoneHome: string;
    fax: string;
    email: string;
    department: string;
    preferredLanguage: string;
    businessPartnerNo: string;
    firstName: string;
    lastName: string;
    title: string;
}
