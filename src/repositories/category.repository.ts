import { isObject } from '../helpers'
import { Category, CategoryGetParams } from '../models'
import { Repository } from '../repository'

export class CategoryRepository extends Repository {
    /**
     * Retrieve the top-level navigation data.
     *
     * @http GET /categories
     * @docs https://support.intershop.com/kb/index.php/Display/2488F4
     * @version <7.9
     */
    getAll(params?: Partial<CategoryGetParams>) {
        return this.http.get<Category[]>('/categories', { params })
    }

    /**
     * Get info on (sub-)category
     *
     * @http GET /categories/<catID>/<subCatID>/...
     * @docs https://support.intershop.com/kb/index.php/Display/24F876
     * @version <7.8
     */
    getById(...args: Array<string | Partial<CategoryGetParams>>) {
        const ids = args.filter(a => typeof a === 'string') as string[]
        const params = (args.find(a => isObject(a)) || {}) as Partial<CategoryGetParams>

        return this.http.get<Category>(`/categories/${ids.join('/')}`, { params })
    }
}
