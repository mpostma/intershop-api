export interface Config {
    host: string;
    production: boolean;
    mock: boolean;
    vat?: number;
    currency?: string;
    debug?: boolean;
}
export declare const defaultConfig: Required<Config>;
