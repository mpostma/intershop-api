import { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import MockAdapter from 'axios-mock-adapter/types';
export interface RequestOptions extends AxiosRequestConfig {
}
export interface HttpResponse<T = any> extends AxiosResponse<T> {
}
export interface HttpHeaders {
    [name: string]: string;
}
export interface Mock extends MockAdapter {
}
export interface Backend extends AxiosInstance {
}
export interface HttpError extends Error {
    response: HttpResponse;
    request: XMLHttpRequest;
    config: RequestOptions;
}
