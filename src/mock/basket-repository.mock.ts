import { randomBetween, randomUuid } from '../helpers'
import { Basket, BasketLineItem, LinkObject, RequestOptions, ResourceCollection, Types } from '../models'
import { BaseMock } from './base.mock'
import { basketFactory, basketLineItemFactory } from './factories'

export class BasketRepositoryMock extends BaseMock {
    create() {
        const id = randomUuid()

        this.mock.onGet(/\/baskets\/\-$/).reply(200, {
            type: 'Link',
            title: id,
            uri: `org-webshop-Site/<site>/baskets/${id}`
        } as LinkObject)
    }

    activeBasket() {
        this.mock.onGet(/\/baskets\/\-$/).reply(200, basketFactory())
    }

    lineItems(basket: Basket) {
        const elements = Array(randomBetween(3, 20)).fill({}).map(basketLineItemFactory)

        this.mock.onGet(/\/baskets\/\S+\/items$/)
            .reply(
                200,
                {
                    name: 'basketLineItems',
                    elements
                } as ResourceCollection<BasketLineItem>
            )
    }

    getItem(basket: Basket, item: BasketLineItem) {
        this.mock.onGet(/\/baskets\/\S+\/items\/\S+$/)
            .reply(
                200,
                basketLineItemFactory(item)
            )
    }

    updateItem(basketId: string, itemId: string, data: any = {}) {
        this.mock.onPut(/\/baskets\/\S+\/items\/\S+$/, data)
            .reply(
                200,
                {
                    type: Types.Link,
                    title: itemId,
                    uri: `org-webshop-Site/<site>/baskets/${basketId}/items/${itemId}`
                } as LinkObject
            )
    }

    removeItem(basketId: string, item: BasketLineItem, options: RequestOptions = {}) {
        this.mock.onDelete(/\/baskets\/\S+\/items\/\S+$/)
            .reply(
                200,
                {
                    id: basketId
                } as Basket
            )
    }
}
