import * as Chance from 'chance'

import { Customer, RequestOptions, Types } from '../models'
import { BaseMock } from './base.mock'

export class CustomerRepositoryMock extends BaseMock {
    authorize(username: string, password: string) {
        this.mock.onGet(/\/customers\/\-$/).reply(200,
            {
                type: Types.Customer,
                email: username,
                firstName: Chance().first(),
                lastName: Chance().last(),
                customerNo: Chance().fbid(),
                phoneHome: Chance().phone(),
                phoneBusiness: Chance().phone(),
                phoneMobile: Chance().phone()
            } as Customer,
            {
                'authentication-token': Chance().apple_token()
            }
        )
    }

    getCustomer(options: RequestOptions = {}) {
        this.mock.onGet(/\/customers\/\-$/).reply(200,
            {
                type: Types.Customer,
                email: Chance().email(),
                firstName: Chance().first(),
                lastName: Chance().last(),
                customerNo: Chance().fbid()
            } as Customer,
            {
                'authentication-token': Chance().apple_token()
            }
        )
    }
}
