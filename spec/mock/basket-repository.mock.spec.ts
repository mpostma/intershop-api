import { Types } from '../../src'
import { BasketRepositoryMock } from '../../src/mock'
import { basketFactory, basketLineItemFactory } from '../../src/mock/factories'
import { BasketRepository } from '../../src/repositories'

describe('BasketRepositoryMock', () => {
    let repository: BasketRepository
    let mock: BasketRepositoryMock

    beforeEach(() => {
        repository = new BasketRepository()
    })

    it('creates', () => {
        expect(new BasketRepositoryMock(repository)).toBeTruthy()
    })

    describe('activeBasket', () => {
        beforeEach(() => {
            repository = new BasketRepository()
            mock = new BasketRepositoryMock(repository)
        })

        describe('run', () => {
            it('returns mocked basket', done => {
                repository.activeBasket().subscribe(res => {
                    expect(res.data.type).toEqual(Types.Basket)
                    expect(res.status).toEqual(200)
                    done()
                })
            })
        })
    })

    describe('lineItems', () => {
        beforeEach(() => {
            repository = new BasketRepository()
            mock = new BasketRepositoryMock(repository)
        })

        describe('run', () => {
            it('returns mocked basket line items', done => {
                repository.lineItems(basketFactory()).subscribe(res => {
                    expect(res.status).toEqual(200)
                    done()
                })
            })
        })
    })

    describe('getItem', () => {
        beforeEach(() => {
            repository = new BasketRepository()
            mock = new BasketRepositoryMock(repository)
        })

        describe('run', () => {
            it('returns mocked basket line item', done => {
                repository.getItem(basketFactory(), basketLineItemFactory()).subscribe(res => {
                    expect(res.data.type).toEqual(Types.BasketLineItem)
                    expect(res.status).toEqual(200)
                    done()
                })
            })
        })
    })

    describe('updateItem', () => {
        beforeEach(() => {
            repository = new BasketRepository()
            mock = new BasketRepositoryMock(repository)
        })

        describe('run', () => {
            it('returns mocked link object', done => {
                repository.updateItem(basketFactory(), basketLineItemFactory()).subscribe(res => {
                    expect(res.data.type).toEqual(Types.Link)
                    expect(res.status).toEqual(200)
                    done()
                })
            })
        })
    })
})
