"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var axios_mock_adapter_1 = require("axios-mock-adapter");
var rxjs_1 = require("rxjs");
var config_1 = require("./config");
exports.defaultHttpHeaders = {
    Accept: 'application/json'
};
var Http = (function () {
    function Http(config) {
        if (config === void 0) { config = config_1.defaultConfig; }
        this._config = config;
        this._backend = axios_1.default.create({
            baseURL: this._config.host,
            headers: { common: exports.defaultHttpHeaders },
        });
        if (this._config.mock) {
            this._mock = new axios_mock_adapter_1.default(this._backend, {
                delayResponse: this.mockDelayTime
            });
        }
    }
    Http.prototype.request = function (options) {
        return rxjs_1.from(this._backend.request(options));
    };
    Http.prototype.get = function (url, options) {
        return this.request(__assign({ method: 'GET', url: url }, options));
    };
    Http.prototype.post = function (url, data, options) {
        return this.request(__assign({ method: 'POST', url: url, data: data }, options));
    };
    Http.prototype.put = function (url, data, options) {
        return this.request(__assign({ method: 'PUT', url: url, data: data }, options));
    };
    Http.prototype.patch = function (url, data, options) {
        return this.request(__assign({ method: 'PATCH', url: url, data: data }, options));
    };
    Http.prototype.delete = function (url, options) {
        return this.request(__assign({ method: 'DELETE', url: url }, options));
    };
    Http.prototype.head = function (url, options) {
        return this.request(__assign({ method: 'HEAD', url: url }, options));
    };
    Object.defineProperty(Http.prototype, "defaultHeaders", {
        get: function () {
            return __assign({}, exports.defaultHttpHeaders, this._backend.defaults.headers.common);
        },
        set: function (value) {
            this._backend.defaults.headers.common = __assign({}, exports.defaultHttpHeaders, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Http.prototype, "mockDelayTime", {
        get: function () {
            return Math.random() * 500;
        },
        enumerable: true,
        configurable: true
    });
    return Http;
}());
exports.Http = Http;
