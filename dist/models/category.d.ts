import { Types } from './types';
export interface Category {
    type: Types.Category;
    id: string;
    name: string;
    description?: string;
    online: string;
    subCategories: Category[];
    subCategoriesCount?: number;
    hasOnlineSubCategories: boolean;
    hasOnlineProducts: boolean;
}
export interface CategoryGetParams {
    view: 'tree';
    limit: number;
    imageView: string;
    imageType: string;
    allImages: boolean;
}
