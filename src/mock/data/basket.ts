export default {
    name: 'basket',
    type: 'Basket',
    commonShipToAddress: {
        type: 'Address',
        city: 'GRONINGEN',
        countryCode: 'NL',
        postalCode: '9717kh',
        street: 'Wallstreet 1337',
        phoneBusiness: '064655545',
        firstName: 'John',
        lastName: 'Doe',
        title: 'Dhr.',
        addressName: 'John Doe, Wallstreet 1337, GRONINGEN',
        id: 'VgcKgiREDxMAAAFqtOEzyTW3',
        country: 'Netherlands'
    },
    commonShippingMethod: {
        type: 'ShippingMethodRO',
        shippingTimeMin: -1,
        shippingTimeMax: -1,
        id: 'shipping-NT-250'
    },
    purchaseCurrency: 'EUR',
    shippingBuckets: [
        {
            name: 'basketShippingBucket',
            type: 'BasketShippingBucket',
            shippingMethod: {
                name: 'Nachtbezorging',
                type: 'BasketShippingMethod',
                shippingTimeMin: -1,
                shippingTimeMax: -1,
                id: 'shipping-NT-250'
            },
            shipToAddress: {
                type: 'Address',
                city: 'GRONINGEN',
                countryCode: 'NL',
                postalCode: '9717kh',
                street: 'Wallstreet 1337',
                phoneBusiness: '064655545',
                firstName: 'John',
                lastName: 'Doe',
                title: 'Dhr.',
                addressName: 'John Doe, Wallstreet 1337, GRONINGEN',
                id: 'VgcKgiREDxMAAAFqtOEzyTW3',
                country: 'Netherlands'
            },
            lineItems: [
                {
                    type: 'Link',
                    uri: 'org-webshop-Site/indi-b2b/baskets/-/items/.AsKgiOhikwAAAFqDsU74bIf',
                    attributes: [
                        {
                            name: 'position',
                            type: 'Integer',
                            value: 3
                        },
                        {
                            name: 'sku',
                            type: 'String',
                            value: '86267'
                        }
                    ]
                },
                {
                    type: 'Link',
                    uri: 'org-webshop-Site/indi-b2b/baskets/-/items/GkQKgiOhq74AAAFqEJI74bIf',
                    attributes: [
                        {
                            name: 'position',
                            type: 'Integer',
                            value: 2
                        },
                        {
                            name: 'sku',
                            type: 'String',
                            value: '86246'
                        }
                    ]
                },
                {
                    type: 'Link',
                    uri: 'org-webshop-Site/indi-b2b/baskets/-/items/WYMKgiOhPHIAAAFqURU74bIN',
                    attributes: [
                        {
                            name: 'position',
                            type: 'Integer',
                            value: 1
                        },
                        {
                            name: 'sku',
                            type: 'String',
                            value: '86271'
                        }
                    ]
                },
                {
                    type: 'Link',
                    uri: 'org-webshop-Site/indi-b2b/baskets/-/items/D58KgiOhcw0AAAFqv8w74bIf',
                    attributes: [
                        {
                            name: 'position',
                            type: 'Integer',
                            value: 4
                        },
                        {
                            name: 'sku',
                            type: 'String',
                            value: '86274'
                        }
                    ]
                }
            ]
        }
    ],
    multipleShippmentsSupported: false,
    dynamicMessages: [],
    salesTaxTotalsByTaxRate: [
        {
            name: 'name',
            type: 'AppliedTax',
            amount: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 29.32
            },
            rate: 21.0
        }
    ],
    shippingTaxTotalsByTaxRate: [
        {
            name: 'name',
            type: 'AppliedTax',
            amount: {
                type: 'Money',
                currencyMnemonic: 'EUR',
                value: 2.26
            },
            rate: 21.0
        }
    ],
    totals: {
        name: 'basketTotals',
        type: 'BasketTotals',
        basketShippingRebatesTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 0.0
        },
        basketTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 150.38
        },
        itemShippingRebatesTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 0.0
        },
        bucketShippingRebatesTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 0.0
        },
        dutiesAndSurchargesTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 0.0
        },
        shippingTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 10.75
        },
        taxTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 31.58
        },
        itemTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 139.63
        },
        basketValueRebatesTotal: {
            type: 'Money',
            currencyMnemonic: 'EUR',
            value: 0.0
        }
    },
    id: 'h0kKgiRE2d4AAAFqa9EzyTW3'
}
