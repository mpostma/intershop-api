import Axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import { from, Observable } from 'rxjs'

import { Config, defaultConfig } from './config'
import { Backend, HttpHeaders, HttpResponse, Mock, RequestOptions } from './models'

export const defaultHttpHeaders: HttpHeaders = {
    Accept: 'application/json'
}

export class Http {
    private readonly _backend: Backend
    private readonly _config: Config
    private readonly _mock?: Mock

    constructor(config: Config = defaultConfig) {
        this._config = config
        this._backend = Axios.create({
            baseURL: this._config.host,
            headers: { common: defaultHttpHeaders },
        })
        if (this._config.mock) {
            this._mock = new MockAdapter(this._backend, {
                delayResponse: this.mockDelayTime
            })
        }
    }

    request<T>(options: RequestOptions) {
        return from(this._backend.request<T>(options)) as Observable<HttpResponse<T>>
    }

    get<T>(url: string, options?: RequestOptions) {
        return this.request<T>({ method: 'GET', url, ...options })
    }

    post<T>(url: string, data?: any, options?: RequestOptions) {
        return this.request<T>({ method: 'POST', url, data, ...options })
    }

    put<T>(url: string, data?: any, options?: RequestOptions) {
        return this.request<T>({ method: 'PUT', url, data, ...options })
    }

    patch<T>(url: string, data?: any, options?: RequestOptions) {
        return this.request<T>({ method: 'PATCH', url, data, ...options })
    }

    delete<T>(url: string, options?: RequestOptions) {
        return this.request<T>({ method: 'DELETE', url, ...options })
    }

    head(url: string, options?: RequestOptions) {
        return this.request({ method: 'HEAD', url, ...options })
    }

    set defaultHeaders(value: HttpHeaders) {
        this._backend.defaults.headers.common = {
            ...defaultHttpHeaders,
            ...value
        }
    }

    get defaultHeaders(): HttpHeaders {
        return {
            ...defaultHttpHeaders,
            ...this._backend.defaults.headers.common,
        }
    }

    private get mockDelayTime() {
        return Math.random() * 500
    }
}
