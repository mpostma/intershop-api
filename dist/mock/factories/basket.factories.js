"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Chance = require("chance");
var helpers_1 = require("../../helpers");
var models_1 = require("../../models");
var common_factories_1 = require("./common.factories");
function basketLineItemFactory(value) {
    if (value === void 0) { value = {}; }
    var quantity = common_factories_1.quantityFactory();
    var singleBasePrice = common_factories_1.moneyFactory();
    var sku = helpers_1.randomUuid().slice(0, 7).toUpperCase();
    var price = common_factories_1.moneyFactory(quantity.value * singleBasePrice.value);
    return __assign({ name: Chance().sentence({ words: 3 }), id: Chance().guid(), type: models_1.Types.BasketLineItem, quantity: quantity,
        singleBasePrice: singleBasePrice,
        price: price, shippingTaxes: [common_factories_1.appliedTaxFactory()], salesTaxes: [common_factories_1.appliedTaxFactory(price.value * .21)], totals: {
            name: 'basketLineItemTotals',
            type: models_1.Types.BasketLineItemTotals,
            salesTaxTotal: common_factories_1.moneyFactory(),
            shippingTaxTotal: common_factories_1.moneyFactory(),
            shippingTotal: common_factories_1.moneyFactory(),
            total: price
        }, isHiddenGift: false, isFreeGift: false, variationProduct: false, bundleProduct: false, availability: true, inStock: true, product: {
            type: models_1.Types.Link,
            title: sku,
            uri: "org-webshop-Site/indi-b2b/products/" + sku,
            attributes: []
        }, position: 2 }, value);
}
exports.basketLineItemFactory = basketLineItemFactory;
function basketFactory() {
    var id = Chance().guid();
    var itemTotal = common_factories_1.moneyFactory();
    var shippingTotal = common_factories_1.moneyFactory(itemTotal.value >= 180 ? 0 : 10.75);
    return {
        name: 'basket',
        id: id,
        type: models_1.Types.Basket,
        externalOrderReferenceID: Chance().guid(),
        department: '',
        lineItems: [],
        shippingRebates: [],
        taxationID: '',
        valueRebates: [],
        invoiceToAddress: common_factories_1.addressFactory(),
        commonShipToAddress: common_factories_1.addressFactory(),
        commonShippingMethod: {
            name: models_1.Types.BasketShippingMethod,
            type: models_1.Types.BasketShippingMethod,
            shippingTimeMin: -1,
            shippingTimeMax: -1,
            id: 'shipping-NT-250'
        },
        purchaseCurrency: 'EUR',
        shippingBuckets: [
            {
                name: 'basketShippingBucket',
                type: models_1.Types.BasketShippingBucket,
                shippingMethod: {
                    name: 'Nachtbezorging',
                    type: models_1.Types.BasketShippingMethod,
                    shippingTimeMin: -1,
                    shippingTimeMax: -1,
                    id: 'shipping-NT-250'
                },
                shipToAddress: common_factories_1.addressFactory(),
                lineItems: [
                    {
                        type: models_1.Types.Link,
                        uri: 'org-webshop-Site/indi-b2b/baskets/-/items/' + Chance().guid(),
                        attributes: [
                            {
                                name: 'position',
                                type: 'Integer',
                                value: 3
                            },
                            {
                                name: 'sku',
                                type: 'String',
                                value: '86267'
                            }
                        ]
                    },
                ]
            }
        ],
        multipleShippmentsSupported: false,
        dynamicMessages: [],
        salesTaxTotalsByTaxRate: [
            {
                name: models_1.Types.AppliedTax,
                type: 'AppliedTax',
                amount: common_factories_1.moneyFactory(),
                rate: 21.0
            }
        ],
        shippingTaxTotalsByTaxRate: [
            {
                name: models_1.Types.AppliedTax,
                type: models_1.Types.AppliedTax,
                amount: common_factories_1.moneyFactory(),
                rate: 21.0
            }
        ],
        totals: {
            name: 'basketTotals',
            type: models_1.Types.BasketTotals,
            basketShippingRebatesTotal: common_factories_1.moneyFactory(),
            basketTotal: common_factories_1.moneyFactory(itemTotal.value),
            itemShippingRebatesTotal: common_factories_1.moneyFactory(),
            bucketShippingRebatesTotal: common_factories_1.moneyFactory(),
            dutiesAndSurchargesTotal: common_factories_1.moneyFactory(),
            shippingTotal: shippingTotal,
            itemTotal: itemTotal,
            basketValueRebatesTotal: common_factories_1.moneyFactory(),
            taxTotal: common_factories_1.moneyFactory(itemTotal.value * 0.21),
        },
    };
}
exports.basketFactory = basketFactory;
