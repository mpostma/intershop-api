declare const _default: {
    elements: {
        name: string;
        type: string;
        quantity: {
            type: string;
            unit: string;
            value: number;
        };
        price: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        singleBasePrice: {
            type: string;
            currencyMnemonic: string;
            value: number;
        };
        shippingTaxes: {
            name: string;
            type: string;
            amount: {
                type: string;
                currencyMnemonic: string;
                value: number;
            };
            rate: number;
        }[];
        salesTaxes: {
            name: string;
            type: string;
            amount: {
                type: string;
                currencyMnemonic: string;
                value: number;
            };
            rate: number;
        }[];
        totals: {
            name: string;
            type: string;
            salesTaxTotal: {
                type: string;
                currencyMnemonic: string;
                value: number;
            };
            shippingTaxTotal: {
                type: string;
                currencyMnemonic: string;
                value: number;
            };
            shippingTotal: {
                type: string;
                currencyMnemonic: string;
                value: number;
            };
            total: {
                type: string;
                currencyMnemonic: string;
                value: number;
            };
        };
        isHiddenGift: boolean;
        isFreeGift: boolean;
        variationProduct: boolean;
        bundleProduct: boolean;
        availability: boolean;
        inStock: boolean;
        product: {
            type: string;
            title: string;
            uri: string;
        };
        position: number;
        id: string;
    }[];
    type: string;
    name: string;
};
export default _default;
