import { Types } from './types';
export interface Credentials {
    username: string;
    password: string;
}
export interface LinkObject {
    type: Types.Link;
    title: string;
    uri: string;
    attributes: {
        name: string;
        type: string;
        value: any;
    }[];
}
export interface ResourceCollection<T = LinkObject> {
    type: Types.ResourceCollection;
    elements: T[];
    name?: string;
}
export interface Address {
    type: Types.Address;
    id: string;
    country: string;
    city: string;
    state?: string;
    postalCode: string;
    street: string;
    street2?: string;
    street3?: string;
    phoneHome?: string;
    phoneBusiness: string;
    firstName: string;
    lastName: string;
    title: string;
    mobile?: string;
    addressName: string;
    countryCode: string;
}
export interface AppliedTax {
    name: Types.AppliedTax;
    type: string;
    amount: Money;
    rate: number;
}
export interface Money {
    type: Types.Money;
    value: number;
    currencyMnemonic: string;
}
