"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var models_1 = require("../models");
var repository_1 = require("../repository");
var CustomerRepository = (function (_super) {
    __extends(CustomerRepository, _super);
    function CustomerRepository() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CustomerRepository.prototype.authorize = function (username, password, options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var credentials = btoa(username + ":" + password);
        return this.http.get('/customers/-', __assign({}, options, { headers: __assign({}, options.headers, { Authorization: "Basic " + credentials }) })).pipe(operators_1.switchMap(function (res) {
            if (res && res.data && res.data.type === models_1.Types.SMBCustomer) {
                return _this.getBusinessCustomer({
                    headers: {
                        'authentication-token': res.headers['authentication-token']
                    }
                });
            }
            return rxjs_1.of(res);
        }));
    };
    CustomerRepository.prototype.create = function (customer) {
        return this.http.post('/customers', customer);
    };
    CustomerRepository.prototype.getCustomer = function (options) {
        if (options === void 0) { options = {}; }
        return this.http.get('/customers/-', options);
    };
    CustomerRepository.prototype.getBusinessCustomer = function (options) {
        return this.http.get('/customers/-/users/-', options);
    };
    CustomerRepository.prototype.addresses = function () {
        return this.http.get('/customers/-/addresses');
    };
    CustomerRepository.prototype.addressDetails = function (addressId) {
        return this.http.get("/customers/-/addresses/" + addressId);
    };
    return CustomerRepository;
}(repository_1.Repository));
exports.CustomerRepository = CustomerRepository;
