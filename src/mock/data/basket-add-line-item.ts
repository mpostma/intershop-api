import { Types } from '../../models'

export default {
    type: Types.Link,
    name: 'Basket',
    uri: ''
}
