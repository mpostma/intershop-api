export declare function isObject(value: any): boolean;
export declare function randomUuid(): string;
export declare function randomBetween(min?: number, max?: number, round?: boolean): number;
export declare function randomFromArray(arr: any[]): any;
