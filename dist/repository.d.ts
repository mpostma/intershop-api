import { Config } from './config';
import { Http } from './http';
export declare abstract class RepositoryBase {
    protected abstract config: Config;
    protected abstract http: Http;
}
export declare class Repository extends RepositoryBase {
    protected readonly http: Http;
    constructor(http?: Http);
    readonly config: Config;
}
