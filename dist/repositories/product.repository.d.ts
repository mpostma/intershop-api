import { ProductSearchParams, ResourceCollection } from '../models';
import { Repository } from '../repository';
export declare class ProductRepository extends Repository {
    getAll(params?: Partial<ProductSearchParams>): import("rxjs").Observable<import("../models").HttpResponse<ResourceCollection<import("../models").LinkObject>>>;
}
