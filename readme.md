# Intershop API client for javascript

This package, which is written in `Typescript`, provides a javascript interface for the Intershop REST api. The official knowledge base documentation of Intershop was used as a source to develop this library.

## Important links

* [Knowledge Base](https://support.intershop.com/kb/index.php)
* [REST Api documentation](https://support.intershop.com/kb/index.php/Display/277A12)

## Dependencies

* [Rxjs](http://reactivex.io/) - Http requests as observable streams
* [Axios](https://github.com/axios/axios) - Handling HTTP requests in NodeJS and browser

---

## Contribute

### Styleguide

Please use the following styleguide:

#### Repositories

It's recommended to create a `repository` for each namespace (after first forward slash), please describe each method by using the following [typedoc](https://typedoc.org) comment markup.

```typescript
/**
 * {METHOD} /{URL}
 * {DESCRIPTION}
 *
 * @docs {URL}
 * @version {VERSION}
 */
```

__Example:__

```typescript
/**
 * POST /customers
 * Creates a private customer.
 *
 * @docs https://support.intershop.com/kb/index.php/Display/X25092
 * @version <7.7
 */
create(data: Data, options: Options) {
    return this.http.post<LinkObject>('customers', data, options)
}
```
