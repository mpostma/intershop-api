import { ProductSearchParams, ResourceCollection } from '../models'
import { Repository } from '../repository'

export class ProductRepository extends Repository {
    /**
     * Get list of items by global product search
     *
     * @http GET /products
     * @docs https://support.intershop.com/kb/index.php/Display/P25172
     */
    getAll(params?: Partial<ProductSearchParams>) {
        return this.http.get<ResourceCollection>('/products', { params })
    }
}
