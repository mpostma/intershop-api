import * as Chance from 'chance'

import { randomBetween } from '../../helpers'
import { Address, AppliedTax, Money, Quantity, Types } from '../../models'

export function moneyFactory(value = randomBetween(1, 1000, false), currencyMnemonic = 'EUR'): Money {
    return {
        type: Types.Money,
        value,
        currencyMnemonic,
    }
}

export function quantityFactory(value = randomBetween(1, 10, true)): Quantity {
    return {
        type: Types.Quantity,
        unit: 'SET',
        value
    }
}

export function appliedTaxFactory(value = randomBetween(1, 1000), rate = 0.21): AppliedTax {
    return {
        type: Types.AppliedTax,
        amount: moneyFactory(value * rate),
        name: Types.AppliedTax,
        rate
    }
}

export function addressFactory(args?: Address): Address {
    const city = Chance().city()
    const street = Chance().street()
    const firstName = Chance().first()
    const lastName = Chance().last()

    return {
        type: Types.Address,
        city,
        countryCode: 'NA',
        postalCode: Chance().zip(),
        street,
        phoneBusiness: Chance().phone(),
        firstName,
        lastName,
        title: Chance().suffix(),
        addressName: `${firstName} ${lastName}, ${street} 1337, ${city}`,
        id: Chance().guid(),
        country: Chance().country(),
        ...args
    }
}
