import { Config, defaultConfig } from '../src/config'
import { Intershop } from '../src/intershop'
import * as repos from '../src/repositories'

describe('Intershop', () => {
    let intershopApi: Intershop
    let config: Config

    beforeEach(() => {
        config = defaultConfig
        intershopApi = new Intershop(config)
    })

    it('creates', () => {
        expect(intershopApi instanceof Intershop).toBeTruthy()
    })

    describe('repositories', () => {
        it('CategoryRepository', () => {
            expect(intershopApi.Category instanceof repos.CategoryRepository).toBeTruthy()
        })
        it('CommonRepository', () => {
            expect(intershopApi.Common instanceof repos.CommonRepository).toBeTruthy()
        })
        it('CustomerRepository', () => {
            expect(intershopApi.Customer instanceof repos.CustomerRepository).toBeTruthy()
        })
        it('BasketRepository', () => {
            expect(intershopApi.Basket instanceof repos.BasketRepository).toBeTruthy()
        })
        it('StoreLocationRepository', () => {
            expect(intershopApi.StoreLocation instanceof repos.StoreLocationRepository).toBeTruthy()
        })
    })
})
