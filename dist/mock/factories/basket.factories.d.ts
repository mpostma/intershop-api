import { Basket, BasketLineItem } from '../../models';
export declare function basketLineItemFactory(value?: Partial<BasketLineItem>): BasketLineItem;
export declare function basketFactory(): Basket;
