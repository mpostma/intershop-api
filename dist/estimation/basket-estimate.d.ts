import { BasketLineItem } from '../models';
export declare function estimateBasketLineItem(item: BasketLineItem): BasketLineItem;
