"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("./http");
var RepositoryBase = (function () {
    function RepositoryBase() {
    }
    return RepositoryBase;
}());
exports.RepositoryBase = RepositoryBase;
var Repository = (function (_super) {
    __extends(Repository, _super);
    function Repository(http) {
        if (http === void 0) { http = new http_1.Http(); }
        var _this = _super.call(this) || this;
        _this.http = http;
        return _this;
    }
    Object.defineProperty(Repository.prototype, "config", {
        get: function () {
            return this.http['_config'];
        },
        enumerable: true,
        configurable: true
    });
    return Repository;
}(RepositoryBase));
exports.Repository = Repository;
