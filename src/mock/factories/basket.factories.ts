import * as Chance from 'chance'

import { randomUuid } from '../../helpers'
import { Basket, BasketLineItem, Types } from '../../models'
import { addressFactory, appliedTaxFactory, moneyFactory, quantityFactory } from './common.factories'

export function basketLineItemFactory(value: Partial<BasketLineItem> = {}): BasketLineItem {
    const quantity = quantityFactory()
    const singleBasePrice = moneyFactory()
    const sku = randomUuid().slice(0, 7).toUpperCase()
    const price = moneyFactory(quantity.value * singleBasePrice.value)

    return {
        name: Chance().sentence({ words: 3 }),
        id: Chance().guid(),
        type: Types.BasketLineItem,
        quantity,
        singleBasePrice,
        price,
        shippingTaxes: [appliedTaxFactory()],
        salesTaxes: [appliedTaxFactory(price.value * .21)],
        totals: {
            name: 'basketLineItemTotals',
            type: Types.BasketLineItemTotals,
            salesTaxTotal: moneyFactory(),
            shippingTaxTotal: moneyFactory(),
            shippingTotal: moneyFactory(),
            total: price
        },
        isHiddenGift: false,
        isFreeGift: false,
        variationProduct: false,
        bundleProduct: false,
        availability: true,
        inStock: true,
        product: {
            type: Types.Link,
            title: sku,
            uri: `org-webshop-Site/indi-b2b/products/${sku}`,
            attributes: []
        },
        position: 2,
        ...value
    }
}

export function basketFactory(): Basket {
    const id = Chance().guid()
    const itemTotal = moneyFactory()
    const shippingTotal = moneyFactory(itemTotal.value >= 180 ? 0 : 10.75)

    return {
        name: 'basket',
        id,
        type: Types.Basket,
        externalOrderReferenceID: Chance().guid(),
        department: '',
        lineItems: [],
        shippingRebates: [],
        taxationID: '',
        valueRebates: [],
        invoiceToAddress: addressFactory(),
        commonShipToAddress: addressFactory(),
        commonShippingMethod: {
            name: Types.BasketShippingMethod,
            type: Types.BasketShippingMethod,
            shippingTimeMin: -1,
            shippingTimeMax: -1,
            id: 'shipping-NT-250'
        },
        purchaseCurrency: 'EUR',
        shippingBuckets: [
            {
                name: 'basketShippingBucket',
                type: Types.BasketShippingBucket,
                shippingMethod: {
                    name: 'Nachtbezorging',
                    type: Types.BasketShippingMethod,
                    shippingTimeMin: -1,
                    shippingTimeMax: -1,
                    id: 'shipping-NT-250'
                },
                shipToAddress: addressFactory(),
                lineItems: [
                    {
                        type: Types.Link,
                        uri: 'org-webshop-Site/indi-b2b/baskets/-/items/' + Chance().guid(),
                        attributes: [
                            {
                                name: 'position',
                                type: 'Integer',
                                value: 3
                            },
                            {
                                name: 'sku',
                                type: 'String',
                                value: '86267'
                            }
                        ]
                    },
                    // {
                    //     type: 'Link',
                    //     uri: 'org-webshop-Site/indi-b2b/baskets/-/items/GkQKgiOhq74AAAFqEJI74bIf',
                    //     attributes: [
                    //         {
                    //             name: 'position',
                    //             type: 'Integer',
                    //             value: 2
                    //         },
                    //         {
                    //             name: 'sku',
                    //             type: 'String',
                    //             value: '86246'
                    //         }
                    //     ]
                    // },
                    // {
                    //     type: 'Link',
                    //     uri: 'org-webshop-Site/indi-b2b/baskets/-/items/WYMKgiOhPHIAAAFqURU74bIN',
                    //     attributes: [
                    //         {
                    //             name: 'position',
                    //             type: 'Integer',
                    //             value: 1
                    //         },
                    //         {
                    //             name: 'sku',
                    //             type: 'String',
                    //             value: '86271'
                    //         }
                    //     ]
                    // },
                    // {
                    //     type: 'Link',
                    //     uri: 'org-webshop-Site/indi-b2b/baskets/-/items/D58KgiOhcw0AAAFqv8w74bIf',
                    //     attributes: [
                    //         {
                    //             name: 'position',
                    //             type: 'Integer',
                    //             value: 4
                    //         },
                    //         {
                    //             name: 'sku',
                    //             type: 'String',
                    //             value: '86274'
                    //         }
                    //     ]
                    // }
                ]
            }
        ],
        multipleShippmentsSupported: false,
        dynamicMessages: [],
        salesTaxTotalsByTaxRate: [
            {
                name: Types.AppliedTax,
                type: 'AppliedTax',
                amount: moneyFactory(),
                rate: 21.0
            }
        ],
        shippingTaxTotalsByTaxRate: [
            {
                name: Types.AppliedTax,
                type: Types.AppliedTax,
                amount: moneyFactory(),
                rate: 21.0
            }
        ],
        totals: {
            name: 'basketTotals',
            type: Types.BasketTotals,
            basketShippingRebatesTotal: moneyFactory(),
            basketTotal: moneyFactory(itemTotal.value),
            itemShippingRebatesTotal: moneyFactory(),
            bucketShippingRebatesTotal: moneyFactory(),
            dutiesAndSurchargesTotal: moneyFactory(),
            shippingTotal,
            itemTotal,
            basketValueRebatesTotal: moneyFactory(),
            taxTotal: moneyFactory(itemTotal.value * 0.21),
        },
    }
}
