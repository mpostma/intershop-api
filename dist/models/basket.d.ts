import { Address, AppliedTax, LinkObject, Money } from './common';
import { RebateTypes, Types } from './types';
export interface Basket {
    type: Types.Basket;
    name: string;
    id: string;
    department: string;
    taxationID: string;
    externalOrderReferenceID: string;
    invoiceToAddress: Address;
    commonShipToAddress?: Address;
    commonShippingMethod?: BasketShippingMethod;
    shippingBuckets: BasketShippingBucket[];
    purchaseCurrency: string;
    multipleShippmentsSupported: boolean;
    totals: BasketTotals;
    salesTaxTotalsByTaxRate: AppliedTax[];
    valueRebates: ValueRebate[];
    shippingRebates: ShippingRebate[];
    shippingTaxTotalsByTaxRate: AppliedTax[];
    dynamicMessages: string[];
    lineItems: LinkObject[];
}
export interface BasketTotals {
    type: Types.BasketTotals;
    name: string;
    basketTotal: Money;
    taxTotal: Money;
    dutiesAndSurchargesTotal: Money;
    shippingTotal: Money;
    itemTotal: Money;
    itemShippingRebatesTotal: Money;
    bucketShippingRebatesTotal: Money;
    basketShippingRebatesTotal: Money;
    basketValueRebatesTotal: Money;
}
export interface ValueRebate {
    type: Types.AppliedRebate;
    rebateType: RebateTypes.OrderValueOffDiscount;
    name: string;
    description: string;
    code: string;
    amount: Money;
}
export interface ShippingRebate {
    type: Types.AppliedRebate;
    rebateType: RebateTypes.ShippingPercentageOffDiscount;
    name: string;
    description: string;
    amount: Money;
}
export interface BasketSkuItem {
    sku: string;
    quantity: {
        value: number;
    };
}
export interface Payment {
    type: Types.Payment;
    name: string;
}
export interface BasketShippingBucket {
    type: Types.BasketShippingBucket;
    name: string;
    shippingMethod: BasketShippingMethod;
    lineItems: Partial<LinkObject>[];
    shipToAddress: Address;
}
export interface BasketShippingMethod {
    type: Types.BasketShippingMethod;
    name: string;
    id: string;
    shippingTimeMin: number;
    shippingTimeMax: number;
}
export interface Quantity {
    type: Types.Quantity;
    unit: string;
    value: number;
}
export interface BasketLineItem {
    type: Types.BasketLineItem;
    id: string;
    name: string;
    quantity: Quantity;
    price: Money;
    singleBasePrice: Money;
    shippingTaxes: AppliedTax[];
    salesTaxes: AppliedTax[];
    totals: {
        name: string;
        type: Types.BasketLineItemTotals;
        salesTaxTotal: Money;
        shippingTaxTotal: Money;
        shippingTotal: Money;
        total: Money;
    };
    isHiddenGift: boolean;
    isFreeGift: boolean;
    variationProduct: boolean;
    bundleProduct: boolean;
    availability: boolean;
    inStock: boolean;
    product: LinkObject;
    position: number;
}
