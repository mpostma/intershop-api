import { Observable } from 'rxjs';
import { Address, BusinessCustomer, Customer, HttpResponse, LinkObject, NewCustomer, RequestOptions, ResourceCollection, SMBCustomer } from '../models';
import { Repository } from '../repository';
export declare class CustomerRepository extends Repository {
    authorize(username: string, password: string, options?: RequestOptions): Observable<HttpResponse<Customer | BusinessCustomer>>;
    create(customer: NewCustomer): Observable<HttpResponse<LinkObject>>;
    getCustomer(options?: RequestOptions): Observable<HttpResponse<Customer | SMBCustomer>>;
    getBusinessCustomer(options?: RequestOptions): Observable<HttpResponse<BusinessCustomer>>;
    addresses(): Observable<HttpResponse<ResourceCollection<LinkObject>>>;
    addressDetails(addressId: string): Observable<HttpResponse<Address>>;
}
