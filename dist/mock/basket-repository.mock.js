"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var helpers_1 = require("../helpers");
var models_1 = require("../models");
var base_mock_1 = require("./base.mock");
var factories_1 = require("./factories");
var BasketRepositoryMock = (function (_super) {
    __extends(BasketRepositoryMock, _super);
    function BasketRepositoryMock() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    BasketRepositoryMock.prototype.create = function () {
        var id = helpers_1.randomUuid();
        this.mock.onGet(/\/baskets\/\-$/).reply(200, {
            type: 'Link',
            title: id,
            uri: "org-webshop-Site/<site>/baskets/" + id
        });
    };
    BasketRepositoryMock.prototype.activeBasket = function () {
        this.mock.onGet(/\/baskets\/\-$/).reply(200, factories_1.basketFactory());
    };
    BasketRepositoryMock.prototype.lineItems = function (basket) {
        var elements = Array(helpers_1.randomBetween(3, 20)).fill({}).map(factories_1.basketLineItemFactory);
        this.mock.onGet(/\/baskets\/\S+\/items$/)
            .reply(200, {
            name: 'basketLineItems',
            elements: elements
        });
    };
    BasketRepositoryMock.prototype.getItem = function (basket, item) {
        this.mock.onGet(/\/baskets\/\S+\/items\/\S+$/)
            .reply(200, factories_1.basketLineItemFactory(item));
    };
    BasketRepositoryMock.prototype.updateItem = function (basketId, itemId, data) {
        if (data === void 0) { data = {}; }
        this.mock.onPut(/\/baskets\/\S+\/items\/\S+$/, data)
            .reply(200, {
            type: models_1.Types.Link,
            title: itemId,
            uri: "org-webshop-Site/<site>/baskets/" + basketId + "/items/" + itemId
        });
    };
    BasketRepositoryMock.prototype.removeItem = function (basketId, item, options) {
        if (options === void 0) { options = {}; }
        this.mock.onDelete(/\/baskets\/\S+\/items\/\S+$/)
            .reply(200, {
            id: basketId
        });
    };
    return BasketRepositoryMock;
}(base_mock_1.BaseMock));
exports.BasketRepositoryMock = BasketRepositoryMock;
