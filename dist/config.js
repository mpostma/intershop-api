"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultConfig = {
    host: '',
    production: false,
    mock: true,
    debug: true,
    vat: 21,
    currency: 'EUR'
};
