import * as deepmerge from 'deepmerge'

import { Config, defaultConfig } from './config'
import { Http } from './http'
import { BasketRepositoryMock, CustomerRepositoryMock, ProductRepositoryMock } from './mock'
import { BaseMock } from './mock/base.mock'
import { HttpError } from './models'
import {
    BasketRepository,
    CategoryRepository,
    CommonRepository,
    CustomerRepository,
    ProductRepository,
    StoreLocationRepository,
} from './repositories'
import { Repository } from './repository'

export class Intershop {
    readonly Basket: BasketRepository
    readonly Category: CategoryRepository
    readonly Common: CommonRepository
    readonly Customer: CustomerRepository
    readonly StoreLocation: StoreLocationRepository
    readonly Product: ProductRepository

    readonly http: Http
    private _config = defaultConfig

    get config() {
        return this._config
    }
    set config(value: Config) {
        this._config = deepmerge.all([this._config, value || {}]) as Required<Config>
    }

    constructor(options: Config) {
        this.config = options
        this.http = new Http(this.config)

        this.Basket = new BasketRepository(this.http)
        this.Category = new CategoryRepository(this.http)
        this.Common = new CommonRepository(this.http)
        this.Customer = new CustomerRepository(this.http)
        this.StoreLocation = new StoreLocationRepository(this.http)
        this.Product = new ProductRepository(this.http)

        if (this.config.mock) {
            this.registerMock(this.Customer, CustomerRepositoryMock)
            this.registerMock(this.Basket, BasketRepositoryMock)
            this.registerMock(this.Product, ProductRepositoryMock)
        }
    }

    interceptError(callback: (error: HttpError) => any | undefined) {
        const backend = this.http['_backend']

        if (backend) {
            backend.interceptors.response.use(
                undefined,
                callback
            )
        }
    }

    private registerMock<S extends Repository, M extends typeof BaseMock>(source: S, mock: M) {
        return new mock(source) as M['prototype']
    }
}
