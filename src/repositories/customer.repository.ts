import { Observable, of } from 'rxjs'
import { switchMap } from 'rxjs/operators'

import {
    Address,
    BusinessCustomer,
    Customer,
    HttpResponse,
    LinkObject,
    NewCustomer,
    RequestOptions,
    ResourceCollection,
    SMBCustomer,
    Types,
} from '../models'
import { Repository } from '../repository'

export class CustomerRepository extends Repository {
    /**
     * Authorize customer with base64 encrypted username and password.
     *
     * @http GET /customers/-
     * @version ??
     */
    authorize(username: string, password: string, options: RequestOptions = {}): Observable<HttpResponse<Customer | BusinessCustomer>> {
        const credentials = btoa(`${username}:${password}`)

        return this.http.get<Customer | SMBCustomer>('/customers/-', {
            ...options,
            headers: {
                ...options.headers,
                Authorization: `Basic ${credentials}`
            }
        }).pipe(
            switchMap(res => {
                if (res && res.data && res.data.type === Types.SMBCustomer) {
                    return this.getBusinessCustomer({
                        headers: {
                            'authentication-token': res.headers['authentication-token']
                        }
                    })
                }
                return of(res as HttpResponse<any>)
            })
        )
    }

    /**
     * Creates a private customer.
     *
     * @http POST /customers
     * @docs https://support.intershop.com/kb/index.php/Display/X25092
     * @version <7.7
     */
    create(customer: NewCustomer) {
        return this.http.post<LinkObject>('/customers', customer)
    }

    /**
     * Gets the profile details of a currently authenticated private customer.
     *
     * @http GET /customers/-
     * @docs https://support.intershop.com/kb/index.php/Display/252P56
     * @version <7.8
     */
    getCustomer(options: RequestOptions = {}) {
        return this.http.get<Customer | SMBCustomer>('/customers/-', options)
    }

    /**
     * Returns the profile details of user of a business customer.
     *
     * @http GET /customers/-/users/-
     * @docs https://support.intershop.com/kb/index.php/Display/2Q5085
     * @version <7.8
     */
    getBusinessCustomer(options?: RequestOptions) {
        return this.http.get<BusinessCustomer>('/customers/-/users/-', options)
    }

    /**
     * Returns a list of links to customer addresses resources.
     *
     * @http GET /customers/-/addresses
     * @docs https://support.intershop.com/kb/index.php/Display/P24900
     * @version <7.7
     */
    addresses() {
        return this.http.get<ResourceCollection>('/customers/-/addresses')
    }

    /**
     * Retrieves and returns details about customer's address to the client.
     *
     * @http GET /customers/-/addresses/<address-id>
     * @docs https://support.intershop.com/kb/index.php/Display/24V916
     * @version <7.6
     */
    addressDetails(addressId: string) {
        return this.http.get<Address>(`/customers/-/addresses/${addressId}`)
    }
}
