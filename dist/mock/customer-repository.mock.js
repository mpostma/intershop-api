"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Chance = require("chance");
var models_1 = require("../models");
var base_mock_1 = require("./base.mock");
var CustomerRepositoryMock = (function (_super) {
    __extends(CustomerRepositoryMock, _super);
    function CustomerRepositoryMock() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CustomerRepositoryMock.prototype.authorize = function (username, password) {
        this.mock.onGet(/\/customers\/\-$/).reply(200, {
            type: models_1.Types.Customer,
            email: username,
            firstName: Chance().first(),
            lastName: Chance().last(),
            customerNo: Chance().fbid(),
            phoneHome: Chance().phone(),
            phoneBusiness: Chance().phone(),
            phoneMobile: Chance().phone()
        }, {
            'authentication-token': Chance().apple_token()
        });
    };
    CustomerRepositoryMock.prototype.getCustomer = function (options) {
        if (options === void 0) { options = {}; }
        this.mock.onGet(/\/customers\/\-$/).reply(200, {
            type: models_1.Types.Customer,
            email: Chance().email(),
            firstName: Chance().first(),
            lastName: Chance().last(),
            customerNo: Chance().fbid()
        }, {
            'authentication-token': Chance().apple_token()
        });
    };
    return CustomerRepositoryMock;
}(base_mock_1.BaseMock));
exports.CustomerRepositoryMock = CustomerRepositoryMock;
