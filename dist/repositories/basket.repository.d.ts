import { Observable } from 'rxjs';
import { Basket, BasketLineItem, BasketSkuItem, HttpResponse, LinkObject, Payment, RequestOptions, ResourceCollection } from '../models';
import { Repository } from '../repository';
export declare class BasketRepository extends Repository {
    create(options?: RequestOptions): Observable<HttpResponse<LinkObject>>;
    activeBasket(options?: RequestOptions): Observable<HttpResponse<Basket>>;
    getById(id: string, options?: RequestOptions): Observable<HttpResponse<Basket>>;
    lineItems(basket: Basket, options?: RequestOptions): Observable<HttpResponse<ResourceCollection<BasketLineItem>>>;
    getItem(basket: Basket, item: BasketLineItem, options?: RequestOptions): Observable<HttpResponse<BasketLineItem>>;
    addItem(basket: Basket, items?: BasketSkuItem[], options?: RequestOptions): Observable<HttpResponse<LinkObject>>;
    updateItem(basket: Basket, item: BasketLineItem, data?: any, options?: RequestOptions): Observable<HttpResponse<LinkObject>>;
    removeItem(basket: Basket, item: BasketLineItem, options?: RequestOptions): Observable<HttpResponse<Basket>>;
    addPayment(basket: Basket, payment: Payment, options?: RequestOptions): Observable<HttpResponse<LinkObject>>;
}
