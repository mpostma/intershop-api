"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var deepmerge = require("deepmerge");
var config_1 = require("./config");
var http_1 = require("./http");
var mock_1 = require("./mock");
var repositories_1 = require("./repositories");
var Intershop = (function () {
    function Intershop(options) {
        this._config = config_1.defaultConfig;
        this.config = options;
        this.http = new http_1.Http(this.config);
        this.Basket = new repositories_1.BasketRepository(this.http);
        this.Category = new repositories_1.CategoryRepository(this.http);
        this.Common = new repositories_1.CommonRepository(this.http);
        this.Customer = new repositories_1.CustomerRepository(this.http);
        this.StoreLocation = new repositories_1.StoreLocationRepository(this.http);
        this.Product = new repositories_1.ProductRepository(this.http);
        if (this.config.mock) {
            this.registerMock(this.Customer, mock_1.CustomerRepositoryMock);
            this.registerMock(this.Basket, mock_1.BasketRepositoryMock);
            this.registerMock(this.Product, mock_1.ProductRepositoryMock);
        }
    }
    Object.defineProperty(Intershop.prototype, "config", {
        get: function () {
            return this._config;
        },
        set: function (value) {
            this._config = deepmerge.all([this._config, value || {}]);
        },
        enumerable: true,
        configurable: true
    });
    Intershop.prototype.interceptError = function (callback) {
        var backend = this.http['_backend'];
        if (backend) {
            backend.interceptors.response.use(undefined, callback);
        }
    };
    Intershop.prototype.registerMock = function (source, mock) {
        return new mock(source);
    };
    return Intershop;
}());
exports.Intershop = Intershop;
