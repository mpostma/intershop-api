import { Mock } from '../models';
import { Repository } from '../repository';
export declare class BaseMock {
    readonly mock: Mock;
    constructor(repository: Repository);
}
