"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var repository_1 = require("../repository");
var StoreLocationRepository = (function (_super) {
    __extends(StoreLocationRepository, _super);
    function StoreLocationRepository() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    StoreLocationRepository.prototype.query = function (params) {
        return this.http.get('stores', { params: params });
    };
    return StoreLocationRepository;
}(repository_1.Repository));
exports.StoreLocationRepository = StoreLocationRepository;
