import { Config } from './config'
import { Http } from './http'

export abstract class RepositoryBase {
    protected abstract config: Config
    protected abstract http: Http
}

export class Repository extends RepositoryBase {
    constructor(protected readonly http = new Http()) {
        super()
    }

    get config() {
        return this.http['_config']
    }
}
