export function isObject(value: any) {
    return typeof value === 'object' && !!value && !Array.isArray(value)
}

export function randomUuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        const r = Math.random() * 16 | 0
        const v = c === 'x' ? r : (r & 0x3 | 0x8)

        return v.toString(16)
    })
}

export function randomBetween(min: number = 1, max: number = 100, round = true) {
    return round
        ? Math.max(min, Math.floor(Math.random() * max))
        : Math.max(min, Math.random() * max)
}

export function randomFromArray(arr: any[]) {
    return arr[Math.floor(Math.random() * arr.length)]
}
