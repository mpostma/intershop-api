import { Address, AppliedTax, Money, Quantity } from '../../models';
export declare function moneyFactory(value?: number, currencyMnemonic?: string): Money;
export declare function quantityFactory(value?: number): Quantity;
export declare function appliedTaxFactory(value?: number, rate?: number): AppliedTax;
export declare function addressFactory(args?: Address): Address;
