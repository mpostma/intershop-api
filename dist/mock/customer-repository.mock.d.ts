import { RequestOptions } from '../models';
import { BaseMock } from './base.mock';
export declare class CustomerRepositoryMock extends BaseMock {
    authorize(username: string, password: string): void;
    getCustomer(options?: RequestOptions): void;
}
