import { BasketLineItem } from '../models'

export function estimateBasketLineItem(item: BasketLineItem) {
    const itemTotal = item.singleBasePrice.value * item.quantity.value

    item.price = {
        ...item.price,
        value: itemTotal
    }
    item.totals.total = {
        ...item.totals.total,
        value: itemTotal
    }

    return item
}
