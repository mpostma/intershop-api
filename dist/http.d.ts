import { Observable } from 'rxjs';
import { Config } from './config';
import { HttpHeaders, HttpResponse, RequestOptions } from './models';
export declare const defaultHttpHeaders: HttpHeaders;
export declare class Http {
    private readonly _backend;
    private readonly _config;
    private readonly _mock?;
    constructor(config?: Config);
    request<T>(options: RequestOptions): Observable<HttpResponse<T>>;
    get<T>(url: string, options?: RequestOptions): Observable<HttpResponse<T>>;
    post<T>(url: string, data?: any, options?: RequestOptions): Observable<HttpResponse<T>>;
    put<T>(url: string, data?: any, options?: RequestOptions): Observable<HttpResponse<T>>;
    patch<T>(url: string, data?: any, options?: RequestOptions): Observable<HttpResponse<T>>;
    delete<T>(url: string, options?: RequestOptions): Observable<HttpResponse<T>>;
    head(url: string, options?: RequestOptions): Observable<HttpResponse<{}>>;
    defaultHeaders: HttpHeaders;
    private readonly mockDelayTime;
}
